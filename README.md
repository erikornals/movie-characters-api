# Movie Characters API

Movie Characters API is a simple API for managing a database of movies, their characters and the franchise they belong to.

## Installation

Install Visual Studio to clone and open the MovieCharacterAPI project. <br> 
Install Microsoft SQLServer Management Studio to explore the databases created. <br>

## Usage

To create the database locally, first go to appsettings.json and replace the name of the database with the name of your local instance. <br>
Then, open you Nuget Package manager console and run command 'Update-Database' to run the database migration. <br>
You will then be able to run the program to see the API Swagger documentation and interface.

## Contributing
Erik Alstad  <br> 
Lasse Steinnes
