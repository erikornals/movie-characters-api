using Microsoft.EntityFrameworkCore;
using MovieCharactersApi.Models;
using MovieCharactersApi.Services.Characters;
using MovieCharactersApi.Services.Franchises;
using MovieCharactersApi.Services.Movies;
using System.Reflection;
using System.Text.Json.Serialization;

namespace MovieCharactersApi
{
    public class Program
    {
        public static void Main(string[] args)
        {
            var builder = WebApplication.CreateBuilder(args);

            //Configuring generated XML docs for Swagger
            var xmlFile = $"{Assembly.GetExecutingAssembly().GetName().Name}.xml";
            var xmlPath = Path.Combine(AppContext.BaseDirectory, xmlFile) ;

            // Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
            builder.Services.AddEndpointsApiExplorer();
            builder.Services.AddSwaggerGen(options =>
            {
                options.SwaggerDoc("v1", new Microsoft.OpenApi.Models.OpenApiInfo
                {
                    Version = "v1",
                    Title = "Movie Characters API",
                    Description = "Simple API to manage movie characters and franchises",
                    Contact = new Microsoft.OpenApi.Models.OpenApiContact
                    {
                        Name = "Erik Alstad",
                        Url = new Uri("https://gitlab.com/erikornals")
                    },
                    License = new Microsoft.OpenApi.Models.OpenApiLicense
                    {
                        Name = "MIT 2023",
                        Url = new Uri("https://opensource.org/licenses/MIT")
                    }
                });
                options.IncludeXmlComments(xmlPath);
            });

            // Add services to the container.

            builder.Services.AddControllers();
            builder.Services.AddDbContext<MovieCharactersDbContext>(
                opt => opt.UseSqlServer(builder.Configuration.GetConnectionString("default")));
            builder.Services.AddScoped<IMovieService, MovieServiceImpl>();
            builder.Services.AddScoped<ICharacterService, CharacterServiceImpl>();
            builder.Services.AddScoped<IFranchiseService, FranchiseServiceImpl>();

            builder.Services.AddControllers().AddJsonOptions(x =>
                x.JsonSerializerOptions.ReferenceHandler = ReferenceHandler.IgnoreCycles);
            builder.Services.AddAutoMapper(AppDomain.CurrentDomain.GetAssemblies());

            var app = builder.Build();

            // Configure the HTTP request pipeline.
            if (app.Environment.IsDevelopment())
            {
                app.UseSwagger();
                app.UseSwaggerUI();
            }

            app.UseHttpsRedirection();

            app.UseAuthorization();

            app.MapControllers();            

            app.Run();
        }
    }
}