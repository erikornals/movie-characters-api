﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Net;
using System.Net.Mime;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using MovieCharactersApi.Models.DTO.Movie;
using MovieCharactersApi.Models.DTOs.Characters;
using MovieCharactersApi.Models.DTOs.Movie;
using MovieCharactersApi.Models.DTOs.Movies;
using MovieCharactersApi.Models.Entities;
using MovieCharactersApi.Services.Movies;
using MovieCharactersApi.Utils.Exceptions;

namespace MovieCharactersApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [ApiConventionType(typeof(DefaultApiConventions))]
    [Produces(MediaTypeNames.Application.Json)]
    [Consumes(MediaTypeNames.Application.Json)]
    public class MoviesController : ControllerBase
    {
        private readonly IMovieService _movieService;
        private readonly IMapper _mapper;

        public MoviesController(IMovieService movieService, IMapper mapper)
        {
            _movieService = movieService;
            _mapper = mapper;
        }

        /// <summary>
        /// Get all movies in the database.
        /// </summary>
        /// <returns></returns>
        // GET: api/Movies
        [HttpGet]
        public async Task<ActionResult<IEnumerable<MovieDTO>>> GetMovies()
        {
            return Ok(
                _mapper.Map<List<MovieDTO>>(
                    await _movieService.GetAllAsync()
                ));
        }

        /// <summary>
        /// Get a movie by its ID.
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        // GET: api/Movies/5
        [HttpGet("{id}")]
        public async Task<ActionResult<MovieDTO>> GetMovie(int id)
        {
            var movie = await _movieService.GetByIdAsync(id);

            if (movie == null)
            {
                return NotFound();
            }

            return Ok(_mapper.Map<MovieDTO>(movie));
        }

        /// <summary>
        /// Get all characters for a movie by Movie ID.
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        // GET: api/Movies/5/Characters
        [HttpGet("{id}/characters")]
        public async Task<ActionResult<IEnumerable<Character>>> GetCharactersForMovie(int id)
        {
            try
            {
                return Ok(
                    _mapper.Map<List<CharacterSummaryDTO>>(
                        await _movieService.GetCharactersAsync(id)
                    ));
            }
            catch (EntityNotFoundException exc)
            {
                return NotFound(
                    new ProblemDetails()
                    {
                        Detail = exc.Message,
                        Status = ((int)HttpStatusCode.NotFound)
                    }
                    );
            }
        }

        /// <summary>
        /// Update a movie to the database.
        /// </summary>
        /// <param name="id"></param>
        /// <param name="movie"></param>
        /// <returns></returns>
        // PUT: api/Movies/5
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [HttpPut("{id}")]
        public async Task<IActionResult> PutMovie(int id, MoviePutDTO movie)
        {
            if (id != movie.Id)
            {
                return BadRequest();
            }
            try
            {
                await _movieService.UpdateAsync(
                        _mapper.Map<Movie>(movie)
                    );
                return NoContent();
            } 
            catch (EntityNotFoundException exc)
            {
                return NotFound(
                    new ProblemDetails()
                    {
                        Detail = exc.Message,
                        Status = ((int)HttpStatusCode.NotFound)
                    }
                    );
            }
        }

        /// <summary>
        /// Update characters for a movie.
        /// </summary>
        /// <param name="characterIds"></param>
        /// <param name="id"></param>
        /// <returns></returns>
        //Put: api/Movies/5/Characters
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [HttpPut("{id}/characters")]
        public async Task<IActionResult> UpdateCharactersForMovie(int[] characterIds, int id)
        {
            try
            {
                await _movieService.UpdateCharactersAsync(characterIds, id);
                return NoContent();
            }
            catch (EntityNotFoundException exc)
            {
                return NotFound(
                    new ProblemDetails()
                    {
                        Detail = exc.Message,
                        Status = ((int)HttpStatusCode.NotFound)
                    }
                    );
            }
        }

        /// <summary>
        /// Add a movie to the database.
        /// </summary>
        /// <param name="movieDto"></param>
        /// <returns></returns>
        // POST: api/Movies
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [HttpPost]
        public async Task<ActionResult<Movie>> PostMovie(MoviePostDTO movieDto)
        {
            Movie movie = _mapper.Map<Movie>(movieDto);

            await _movieService.AddAsync(movie);

            return CreatedAtAction("GetMovie", new { id = movie.Id }, movie);
        }

        /// <summary>
        /// Delete a movie from the database.
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        // DELETE: api/Movies/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteMovie(int id)
        {
            try
            {
                await _movieService.DeleteByIdAsync(id);
                return NoContent();
            }
            catch(EntityNotFoundException exc)
            {
                return NotFound(
                    new ProblemDetails()
                    {
                        Detail = exc.Message,
                        Status = ((int) HttpStatusCode.NotFound)
                    }
                    );
            }
        }
    }
}
