﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mime;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using MovieCharactersApi.Models.Entities;
using MovieCharactersApi.Services.Characters;
using MovieCharactersApi.Services.Franchises;
using MovieCharactersApi.Models.DTOs.Characters;
using AutoMapper;
using MovieCharactersApi.Utils.Exceptions;

namespace MovieCharactersApi.Controllers
{
    /// <summary>
    /// Controller for characters domain
    /// </summary>

    [Route("api/[controller]")]
    [ApiController]
    [ApiConventionType(typeof(DefaultApiConventions))]
    [Produces(MediaTypeNames.Application.Json)]
    [Consumes(MediaTypeNames.Application.Json)]
    public class CharactersController : ControllerBase
    {
        private readonly ICharacterService _characterService;
        private readonly IMapper _mapper;


        public CharactersController(ICharacterService characterService, IMapper mapper)
        {
            _characterService = characterService;
            _mapper = mapper;
        }

        // GET: api/Characters
        /// <summary>
        /// Get all the characters in the database
        /// </summary>
        /// <returns> List of CharacterDto </returns>
        [HttpGet]
        public async Task<ActionResult<IEnumerable<CharacterDto>>> GetCharacters()
        {
            return Ok(
                _mapper.Map<List<CharacterDto>>(
                    await _characterService.GetAllAsync())
                );
        }

        // GET: api/Characters/5
        /// <summary>
        ///  Get a character in the database based on character ID
        /// </summary>
        /// <param name="id"></param>
        /// <returns> A characterDto </returns>
        [HttpGet("{id}")]
        public async Task<ActionResult<CharacterDto>> GetCharacter(int id)
        {
            try
            {
                return Ok(_mapper.Map<CharacterDto>(
                    await _characterService.GetByIdAsync(id))
                    );
            }
            catch (EntityNotFoundException ex) 
            {
                    return NotFound();
            }
        }

        // PUT: api/Characters/5
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        /// <summary>
        /// Update a character with the given character ID
        /// </summary>
        /// <param name="id"></param>
        /// <param name="character"></param>
        /// <returns> No content </returns>
        [HttpPut("{id}")]
        public async Task<IActionResult> PutCharacter(int id, CharacterPutDto character)
        {
            if (id != character.Id)
            {
                return BadRequest();
            }

            try
            {
                await _characterService.UpdateAsync(_mapper.Map<Character>(character)
                    );
            }
            catch (EntityNotFoundException ex)
            {
                // throw;
            }

            return NoContent();
        }

        // PUT: api/v1/Characters/3/movies
        /// <summary>
        /// Update movies for a character.
        /// </summary>
        /// <param name="id"></param>
        /// <param name="movieIds"></param>
        /// <returns> No content </returns>
        [HttpPut("{id}/movies")]
        public async Task<IActionResult> UpdateMoviesForCharacter (int id, int[] movieIds)
        {
            try
            {
                await _characterService.UpdateMoviesAsync(movieIds, id);
                return NoContent();
            }
            catch (EntityNotFoundException ex)
            {
                return NotFound();
            }
        }

        // POST: api/Characters
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        /// <summary>
        /// Post a new character
        /// </summary>
        /// <param name="characterDto"></param>
        /// <returns> Createrespons for the newly created character </returns>
        [HttpPost]
        public async Task<ActionResult<Character>> PostCharacter(CharacterPostDto characterDto)
        {
            Character character = _mapper.Map<Character>(characterDto);
            await _characterService.AddAsync(character);

            return CreatedAtAction("GetCharacter", new { id = character.Id }, character);
        }

        // DELETE: api/Characters/5
        /// <summary>
        /// Delete a character given by the character ID
        /// </summary>
        /// <param name="id"></param>
        /// <returns> No content </returns>
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteCharacter(int id)
        {
            try
            {
                await _characterService.DeleteByIdAsync(id);
                return NoContent();
            }
            catch(EntityNotFoundException ex)
            {
                // throw
                return NotFound();
            }
         
        }
    }
}
