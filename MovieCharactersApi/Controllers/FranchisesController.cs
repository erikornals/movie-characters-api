﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mime;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using MovieCharactersApi.Models.Entities;
using MovieCharactersApi.Services.Franchises;
using MovieCharactersApi.Models.DTOs.Franchises;
using MovieCharactersApi.Models.DTOs.Movies;
using MovieCharactersApi.Utils.Exceptions;
using AutoMapper;
using MovieCharactersApi.Models.DTOs.Characters;

namespace MovieCharactersApi.Controllers
{
    /// <summary>
    ///  Controller for Franchise domain
    /// </summary>
    [Route("api/[controller]")] // franchises
    [ApiController]
    [ApiConventionType(typeof(DefaultApiConventions))]
    [Produces(MediaTypeNames.Application.Json)]
    [Consumes(MediaTypeNames.Application.Json)]
    // [ApiConventionType(typeof(DefaultApiConventions))]

    public class FranchisesController : ControllerBase
    {
        // private readonly IMapper _mapper;
        private readonly IFranchiseService _franchiseService;
        private readonly IMapper _mapper;

        public FranchisesController(IFranchiseService franchiseService, IMapper mapper)
        {
            _franchiseService = franchiseService;
            _mapper = mapper;
            
        }

        // GET: api/Franchises
        /// <summary>
        /// Get all the franchises in the database.
        /// </summary>
        /// <returns> List of franchiseDto </returns>
        [HttpGet]
        public async Task<ActionResult<IEnumerable<Franchise>>> GetFranchises()
        {
            return Ok(
                _mapper.Map<List<FranchiseDto>>(
                    await _franchiseService.GetAllAsync())
                );
        }

        // GET: api/Franchises/5
        /// <summary>
        /// Get a franchise from its ID
        /// </summary>
        /// <param name="id"></param>
        /// <returns> FranchiseDto </returns>
        [HttpGet("{id}")]
        public async Task<ActionResult<FranchiseDto>> GetFranchise(int id)
        {
            try
            {
                return Ok(_mapper.Map<FranchiseDto>(
                    await _franchiseService.GetByIdAsync(id))
                    );
            }
            
            catch (EntityNotFoundException ex)
            {
                // Throw
                return NotFound();
            }
        }

        // GET: api/v1/franchises/2/movies
        /// <summary>
        /// Get movies for a Franchise from a given Franchise ID
        /// </summary>
        /// <param name="id"></param>
        /// <returns> List of MovieSummaryDto</returns>
        [HttpGet("{id}/movies")]
        public async Task<ActionResult<IEnumerable<MovieSummaryDto>>> GetMoviesForFranchise(int id)
        {
            return Ok(
                _mapper.Map<List<MovieSummaryDto>>(
                    await _franchiseService.GetMoviesAsync(id))
                );
        }

        // GET: api/v1/franchises/2/characters
        /// <summary>
        /// Get all the characters for a franchise, from a franchise ID
        /// </summary>
        /// <param name="id"></param>
        /// <returns> List of CharactersSummaryDto </returns>
        [HttpGet("{id}/characters")]
        public async Task<ActionResult<IEnumerable<CharacterSummaryDTO>>> GetCharactersForFranchise(int id)
        {
            return Ok(
                _mapper.Map<List<CharacterSummaryDTO>>(
                    await _franchiseService.GetCharactersAsync(id))
                );
        }

        // PUT: api/Franchises/5
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        /// <summary>
        /// Update a franchise
        /// </summary>
        /// <param name="id"></param>
        /// <param name="franchise"></param>
        /// <returns> No content </returns>
        [HttpPut("{id}")]
        public async Task<IActionResult> PutFranchise(int id, FranchisePutDto franchise)
        {
            if (id != franchise.Id)
            {
                return BadRequest();
            }

            try
            {
                await _franchiseService.UpdateAsync(_mapper.Map<Franchise>(franchise)
                    );
            }
            catch (DbUpdateConcurrencyException)
            {
                // throw
            }

            return NoContent();
        }

        // PUT: api/v1/Franchises/3/movies
        /// <summary>
        /// Update movies for a franchise determined by Franchise ID
        /// </summary>
        /// <param name="id"></param>
        /// <param name="movieIds"></param>
        /// <returns> No content </returns>
        [HttpPut("{id}/movies")] 
        public async Task<IActionResult> UpdateMoviesForFranchise(int id, int[] movieIds)
        {
            await _franchiseService.UpdateMoviesAsync(movieIds, id);

            return NoContent();
        }

        // POST: api/v1/Franchises/3
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        /// <summary>
        /// Post a new franchise.
        /// </summary>
        /// <param name="franchiseDto"></param>
        /// <returns> Status created response for the newly created Franchise </returns>
        [HttpPost]
        public async Task<ActionResult<Franchise>> PostFranchise(FranchisePostDto franchiseDto)
        {
            Franchise franchise = _mapper.Map<Franchise>(franchiseDto);
            await _franchiseService.AddAsync(franchise);
            
            return CreatedAtAction("GetFranchise", new { id = franchise.Id }, franchise);
        }

        // DELETE: api/Franchises/5
        /// <summary>
        /// Delete a franchise for a given Franchise ID
        /// </summary>
        /// <param name="id"></param>
        /// <returns> No content </returns>
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteFranchise(int id)
        {
            await _franchiseService.DeleteByIdAsync(id);
            return NoContent();
        }
    }
}
