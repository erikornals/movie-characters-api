﻿using AutoMapper;
using MovieCharactersApi.Models.DTO.Movie;
using MovieCharactersApi.Models.DTOs.Characters;
using MovieCharactersApi.Models.DTOs.Movie;
using MovieCharactersApi.Models.DTOs.Movies;
using MovieCharactersApi.Models.Entities;

namespace MovieCharactersApi.Profiles
{
    /// <summary>
    /// Mappings for Movie entity to various DTOs.
    /// </summary>
    public class MovieProfile : Profile
    {
        public MovieProfile()
        {
            CreateMap<Movie, MovieDTO>()
                .ForMember(dto => dto.Franchise, opt => opt
                .MapFrom(p => p.FranchiseId))
                .ForMember(dto => dto.Characters, opt => opt
                .MapFrom(p => p.Characters.Select(c => c.Id).ToList()))
                .ReverseMap();

            CreateMap<MoviePutDTO, Movie>();
            CreateMap<MoviePostDTO, Movie>();

            //CreateMap<MoviePostDTO, Movie>();
            CreateMap<Movie, MovieSummaryDto>();
        }
    }
}
