﻿using AutoMapper;
using MovieCharactersApi.Models.DTOs.Franchises;
using MovieCharactersApi.Models.Entities;

namespace MovieCharactersApi.Profiles
{
    /// <summary>
    ///  Mapping of characters entity to Dtos
    /// </summary>
    public class FranchiseProfile : Profile
    {
        public FranchiseProfile()
        {
            CreateMap<FranchisePostDto, Franchise>(); // <source,destination>

            CreateMap<Franchise, FranchiseDto>()
                .ForMember(dto => dto.Movies, opt => opt
                .MapFrom(c => c.Movies.Select(m => m.Id).ToList()));

            CreateMap<FranchisePutDto, Franchise>();
        }
    }
}


