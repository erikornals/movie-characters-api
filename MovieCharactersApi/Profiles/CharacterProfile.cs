﻿using AutoMapper;
using MovieCharactersApi.Models.Entities;
using MovieCharactersApi.Models.DTOs.Characters;

namespace MovieCharactersApi.Profiles
{   
    /// <summary>
    ///  Mapping o characters entity to Dtos
    /// </summary>
    public class CharacterProfile : Profile
    {
        public CharacterProfile() 
        {
            CreateMap<CharacterPostDto, Character>(); // <source,destination>

            CreateMap<Character, CharacterDto>()
                .ForMember(dto => dto.Movies, opt => opt
                .MapFrom(c => c.Movies.Select(m => m.Id).ToList()));

            CreateMap<CharacterPutDto, Character>();
            CreateMap<Character, CharacterSummaryDTO>();
        }
    }
}
