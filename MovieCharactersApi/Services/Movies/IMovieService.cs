﻿using MovieCharactersApi.Models.Entities;

namespace MovieCharactersApi.Services.Movies
{
    public interface IMovieService : ICrudService<Movie, int>
    {
        /// <summary>
        ///  Get all characters associated with a movie
        /// </summary>
        /// <param name="movieId"></param>
        /// <returns></returns>
        Task<ICollection<Character>> GetCharactersAsync(int movieId);

        /// <summary>
        /// Update the characters associated with a movieId
        /// </summary>
        /// <param name="characterIds"></param>
        /// <param name="movieId"></param>
        /// <returns></returns>
        Task UpdateCharactersAsync(int[] characterIds, int movieId);
    }
}
