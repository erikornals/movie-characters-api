﻿using Microsoft.EntityFrameworkCore;
using MovieCharactersApi.Models;
using MovieCharactersApi.Models.Entities;
using MovieCharactersApi.Utils.Exceptions;
using System.ComponentModel.DataAnnotations;
using System.Reflection;

namespace MovieCharactersApi.Services.Movies
{
    public class MovieServiceImpl : IMovieService
    {
        private readonly MovieCharactersDbContext _context;
        private readonly ILogger<MovieServiceImpl> _logger;

        public MovieServiceImpl(MovieCharactersDbContext context, ILogger<MovieServiceImpl> logger)
        {
            _context = context;
            _logger = logger;
        }

        public async Task<ICollection<Movie>> GetAllAsync()
        {
            return await _context.Movies
                .Include(m => m.Characters)
                .Include(m => m.Franchise)
                .ToListAsync();
        }

        public async Task<Movie> GetByIdAsync(int id)
        {
            if (!await MovieExistsAsync(id))
            {
                _logger.LogError("Movie not found with Id: " + id);
                throw new MovieNotFoundException();
            }
            // Want to include all related data for professor
            return await _context.Movies
                .Where(m => m.Id == id)
                .Include(m => m.Characters)
                .Include(m => m.Franchise)
                .FirstAsync();
        }

        public async Task AddAsync(Movie entity)
        {
            await _context.Movies.AddAsync(entity);
            await _context.SaveChangesAsync();
        }

        public async Task UpdateAsync(Movie entity)
        {
            if (!await MovieExistsAsync(entity.Id))
            {
                _logger.LogError("Movie not found with Id: " + entity.Id);
                throw new MovieNotFoundException();
            }

            //Ignores updates to Foreign keys, which are handled via separate endpoints
            //Properties specified to keep existing Foreign keys
            _context.Entry(entity).Property(e => e.Title).IsModified = true;
            _context.Entry(entity).Property(e => e.Description).IsModified = true;
            _context.Entry(entity).Property(e => e.Genre).IsModified = true;
            _context.Entry(entity).Property(e => e.ReleaseYear).IsModified = true;
            _context.Entry(entity).Property(e => e.Director).IsModified = true;
            _context.Entry(entity).Property(e => e.Picture).IsModified = true;
            _context.Entry(entity).Property(e => e.Trailer).IsModified = true;

            await _context.SaveChangesAsync();
        }

        public async Task DeleteByIdAsync(int id)
        {
            var movie = await _context.Movies.FindAsync(id);
            if (movie == null)
            {
                _logger.LogError("Movie not found with Id: " + id);
                throw new MovieNotFoundException();
            }

            _context.Movies.Remove(movie);
            await _context.SaveChangesAsync();
        }

        public async Task<ICollection<Character>> GetCharactersAsync(int movieId)
        {
            return (await _context.Movies
                .Where(m => m.Id == movieId)
                .Include(m => m.Characters)
                .FirstAsync()).Characters;
        }

        public async Task UpdateCharactersAsync(int[] characterIds, int movieId)
        {
            if (!await MovieExistsAsync(movieId))
            {
                _logger.LogError("Movie not found with Id: " + movieId);
                throw new MovieNotFoundException();
            }

            List<Character> characters = characterIds
                .ToList()
                .Select(cid => _context.Characters
                .Where(c => c.Id == cid).First())
                .ToList();
            Movie movie = await _context.Movies
                .Where(m => m.Id == movieId)
                .FirstAsync();
            movie.Characters = characters;
            _context.Entry(movie).State = EntityState.Modified;
            await _context.SaveChangesAsync();
        }

        /// <summary>
        /// Simple utility method to help check if a movie exists with provided Id.
        /// </summary>
        /// <param name="id"></param>
        /// <returns>If movie exists</returns>
        private async Task<bool> MovieExistsAsync(int id)
        {
            return await _context.Movies.AnyAsync(m => m.Id == id);
        }

    }
}
