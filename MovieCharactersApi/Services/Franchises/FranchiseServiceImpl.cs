﻿using Microsoft.EntityFrameworkCore;
using MovieCharactersApi.Models.Entities;
using MovieCharactersApi.Models;
using MovieCharactersApi.Models.Entities;
using MovieCharactersApi.Services.Characters;
// using MovieCharactersApi.Services.Movie;

namespace MovieCharactersApi.Services.Franchises
{
    public class FranchiseServiceImpl : IFranchiseService
    {
        /// <summary>
        ///  Serves business logic for franchise
        /// </summary>
        /// <param name="franchiseId"></param>
        /// <returns></returns>

        private readonly MovieCharactersDbContext _postgradEfContext;
        private readonly ILogger<CharacterServiceImpl> _logger;

        public FranchiseServiceImpl(MovieCharactersDbContext postgradEf, ILogger<CharacterServiceImpl> logger)
        {
            _postgradEfContext = postgradEf;
            _logger = logger;
        }

        // Add
        public async Task AddAsync(Franchise entity)
        {
            await _postgradEfContext.AddAsync(entity);
            await _postgradEfContext.SaveChangesAsync();
        }


        // Get
        // franchises and movies associated with franchises
        public async Task<ICollection<Franchise>> GetAllAsync()
        {
            return await _postgradEfContext.Franchises
                .Include(c => c.Movies)
                .ToListAsync();
        }

        public async Task<Franchise> GetByIdAsync(int id)
        {
            if (!await FranchisesExistsAsync(id))
            {
                _logger.LogError("Franchise not found with Id: " + id);
                // throw new notfoundexception();
            }

            return await _postgradEfContext.Franchises
                .Where(f => f.Id == id)
                .Include(f => f.Movies)
                .FirstAsync();
        }

        public async Task<ICollection<Movie>> GetMoviesAsync(int franchiseId)
        {
            if (!await FranchisesExistsAsync(franchiseId))
            {
                _logger.LogError("Franchise not found with Id: " + franchiseId);
                // throw new notfoundexception();
            }

            return await _postgradEfContext.Movies
                .Where(f => f.Id == franchiseId)
                .ToListAsync();
        }

        public async Task<ICollection<Character>> GetCharactersAsync(int franchiseId)
        {
            if (!await FranchisesExistsAsync(franchiseId))
            {
                _logger.LogError("Franchise not found with Id: " + franchiseId);
                // throw new notfoundexception();
            }

            return await _postgradEfContext.Characters
                .Where(f => f.Id == franchiseId)
                .ToListAsync();
        }

        // Update
        public async Task UpdateAsync(Franchise entity)
        {
            if (!await FranchisesExistsAsync(entity.Id))
            {
                _logger.LogError("Franchise not found with Id: " + entity.Id);
                // throw new notfoundexception();
            }
            _postgradEfContext.Entry(entity).State = EntityState.Modified;
            await _postgradEfContext.SaveChangesAsync();
        }

        public async Task UpdateMoviesAsync(int[] movieIds, int franchiseId)
        {
            if (!await FranchisesExistsAsync(franchiseId))
            {
                _logger.LogError("Franchise not found with Id: " + franchiseId);
                // throw new notfoundexception();
            }

            // Make movies id into a list - ps: could add exception handling here
            //  i.e. MoviesNotFoundException
            List<Movie> movies = movieIds
                .ToList()
                .Select(mid => _postgradEfContext.Movies
                .Where(m => m.Id == mid).First())
                .ToList();

            // Get Franchise from franchiseId
            Franchise franchise = await _postgradEfContext.Franchises
                .Where(f => f.Id == franchiseId)
                .FirstAsync();

            // set movies for the character
            franchise.Movies = movies;
            _postgradEfContext.Entry(franchise).State = EntityState.Modified;

            //Saving changes
            await _postgradEfContext.SaveChangesAsync();
        }


        // Delete 
        public async Task DeleteByIdAsync(int id)
        {
            var franchise = await _postgradEfContext.Franchises.FindAsync(id);

            if (franchise == null)
            {
                _logger.LogError("Franchise not found with Id: " + id);
                // throw new ProfessorNotFoundException();
            }
            // entities must be set to have nullable RELATIONSHIPS
            // removes foreignkeys when deleted
            _postgradEfContext.Franchises.Remove(franchise);
            await _postgradEfContext.SaveChangesAsync();
        }

        private async Task<bool> FranchisesExistsAsync(int id)
        {
            return await _postgradEfContext.Franchises.AnyAsync(c => c.Id == id);
        }
    }
}
