﻿using MovieCharactersApi.Models.Entities;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace MovieCharactersApi.Services.Franchises
{
    public interface IFranchiseService : ICrudService<Franchise, int>
	{
        /// <summary>
        ///  Get all the movies associated with a franchise
        /// </summary>
        /// <param name="franchiseId"></param>
        /// <returns></returns>
        Task<ICollection<Movie>> GetMoviesAsync(int franchiseId);
        Task<ICollection<Character>> GetCharactersAsync(int franchiseId);
        /// <summary>
        /// Update the movies associated with a franchiseId
        /// </summary>
        /// <param name="movieIds"></param>
        /// <param name="franchiseId"></param>
        /// <returns></returns>
        Task UpdateMoviesAsync(int[] movieIds, int franchiseId);
    }
}
