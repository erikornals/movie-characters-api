﻿using Microsoft.EntityFrameworkCore;
using MovieCharactersApi.Models;
using MovieCharactersApi.Models.Entities;
using System.Linq;
using System.Threading.Tasks;

namespace MovieCharactersApi.Services.Characters
{
    public class CharacterServiceImpl : ICharacterService 
	{ 
	/// <summary>
	/// Class to house business logic
	/// Interacts with entity framework
	/// Service will be injectable and used in Controller
	/// Context refers to database
	/// </summary>
	/// 
		private readonly MovieCharactersDbContext _postgradEfContext;
		private readonly ILogger<CharacterServiceImpl> _logger; 
	
		public CharacterServiceImpl(MovieCharactersDbContext postgradEf, ILogger<CharacterServiceImpl> logger)
		{
			_postgradEfContext= postgradEf;
			_logger = logger;
		}

		// Add
		public async Task AddAsync(Character entity)
		{
			await _postgradEfContext.AddAsync(entity);
			await _postgradEfContext.SaveChangesAsync();
		}


		// Get 
		public async Task<ICollection<Character>> GetAllAsync()
		{
			return await _postgradEfContext.Characters
				.Include(c => c.Movies)
				.ToListAsync();
		}

		public async Task<Character> GetByIdAsync(int id)
		{
			if (!await CharacterExistsAsync(id))
			{
                _logger.LogError("Character not found with Id: " + id);
				// throw new notfoundexception();
            }

			return await _postgradEfContext.Characters
				.Where(c => c.Id == id)
				.Include(c => c.Movies)
				.FirstAsync();
		}

		public async Task<ICollection<Movie>> GetMoviesAsync(int characterId)
		{
			if (!await CharacterExistsAsync(characterId))
			{
                _logger.LogError("Character not found with Id: " + characterId);
                // throw new notfoundexception();
            }

            return (ICollection<Movie>) _postgradEfContext.Characters
				.Where(c => c.Id == characterId)
				.Include(c => c.Movies)
                .ToListAsync();

            /* return (ICollection<Movie>) _postgradEfContext.Characters
				.Where(m => m.Id == characterId)
				.Include(m => m.Movies)
				.ToListAsync()*/
        }


		// Update
		 public async Task UpdateAsync(Character entity)
		{
            if (!await CharacterExistsAsync(entity.Id))
            {
                _logger.LogError("Character not found with Id: " + entity.Id);
                // throw new notfoundexception();
            }
			_postgradEfContext.Entry(entity).State = EntityState.Modified;
			await _postgradEfContext.SaveChangesAsync();
        }

		public async Task UpdateMoviesAsync(int[] moviesIds, int characterId)
		{
			if (!await CharacterExistsAsync(characterId))
			{
                _logger.LogError("Character not found with Id: " + characterId);
                // throw new notfoundexception();
            }

			// Make movies id into a list - ps: could add exception handling here
			//  i.e. MoviesNotFoundException
			List<Movie> movies = moviesIds
				.ToList()
				.Select(mid => _postgradEfContext.Movies
				.Where(m => m.Id == mid).First())
				.ToList();

			// Get Character from characterId
			Character character = await _postgradEfContext.Characters
				.Where(c => c.Id == characterId)
				.FirstAsync();

			// set movies for the character
			character.Movies = movies;
			_postgradEfContext.Entry(character).State = EntityState.Modified;
			
			//Saving changes
			await _postgradEfContext.SaveChangesAsync();
        }
		
		// Delete character
		public async Task DeleteByIdAsync(int id)
		{
			var character = await _postgradEfContext.Characters.FindAsync(id);

			if (character == null) 
			{
                _logger.LogError("Character not found with Id: " + id);
                // throw new ProfessorNotFoundException();
            }
			// entities must be set to have nullable RELATIONSHIPS
			// removes foreignkeys when deleted
			_postgradEfContext.Characters.Remove(character);
			await _postgradEfContext.SaveChangesAsync();
        }

		/// <summary>
		/// private utility method to check if id exists
		/// </summary>
		private async Task<bool> CharacterExistsAsync(int id)
		{
			return await _postgradEfContext.Characters.AnyAsync(c => c.Id == id);
		}
	}
}
