﻿using MovieCharactersApi.Models.Entities;

namespace MovieCharactersApi.Services.Characters
{
    /// <summary>
    /// Service interface for characters
    /// </summary>
    public interface ICharacterService : ICrudService<Character,int>
	{
        /// <summary>
        ///  Get all the movies related to a characters id
        /// </summary>
        /// <param name="characterId"></param>
        /// <returns></returns>
        Task<ICollection<Movie>> GetMoviesAsync(int characterId);

        /// <summary>
        /// Update the movies associated to a movies ids for a specific character
        /// </summary>
        /// <param name="movieIds"></param>
        /// <param name="characterId"></param>
        /// <returns></returns>
        Task UpdateMoviesAsync(int[] movieIds,int characterId);
    }
}