﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

#pragma warning disable CA1814 // Prefer jagged arrays over multidimensional

namespace MovieCharactersApi.Migrations
{
    /// <inheritdoc />
    public partial class InitialDb : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Character",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Name = table.Column<string>(type: "nvarchar(50)", maxLength: 50, nullable: false),
                    Alias = table.Column<string>(type: "nvarchar(50)", maxLength: 50, nullable: true),
                    Gender = table.Column<string>(type: "nvarchar(50)", maxLength: 50, nullable: true),
                    Picture = table.Column<string>(type: "nvarchar(max)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Character", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Franchise",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Name = table.Column<string>(type: "nvarchar(50)", maxLength: 50, nullable: true),
                    Description = table.Column<string>(type: "nvarchar(500)", maxLength: 500, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Franchise", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Movie",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Title = table.Column<string>(type: "nvarchar(50)", maxLength: 50, nullable: false),
                    Description = table.Column<string>(type: "nvarchar(500)", maxLength: 500, nullable: true),
                    Genre = table.Column<string>(type: "nvarchar(50)", maxLength: 50, nullable: true),
                    ReleaseYear = table.Column<int>(type: "int", nullable: false),
                    Director = table.Column<string>(type: "nvarchar(50)", maxLength: 50, nullable: false),
                    Picture = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Trailer = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    FranchiseId = table.Column<int>(type: "int", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Movie", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Movie_Franchise_FranchiseId",
                        column: x => x.FranchiseId,
                        principalTable: "Franchise",
                        principalColumn: "Id");
                });

            migrationBuilder.CreateTable(
                name: "CharacterMovie",
                columns: table => new
                {
                    MoviesId = table.Column<int>(type: "int", nullable: false),
                    CharactersId = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CharacterMovie", x => new { x.MoviesId, x.CharactersId });
                    table.ForeignKey(
                        name: "FK_CharacterMovie_Character_CharactersId",
                        column: x => x.CharactersId,
                        principalTable: "Character",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_CharacterMovie_Movie_MoviesId",
                        column: x => x.MoviesId,
                        principalTable: "Movie",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.InsertData(
                table: "Character",
                columns: new[] { "Id", "Alias", "Gender", "Name", "Picture" },
                values: new object[,]
                {
                    { 1, "Mad Max", "Male", "Max Rockatansky", "https://upload.wikimedia.org/wikipedia/commons/8/80/Long_Beach_Comic_%26_Horror_Con_2011_-_Mad_Max_%286301702996%29.jpg" },
                    { 2, "Ripley", "Female", "Lt. Ellen Louise Ripley", "https://upload.wikimedia.org/wikipedia/en/b/b9/Ellen_Ripley_%28Alien_-_Isolation%29.jpg" },
                    { 3, "The Love Bug", "Car", "Herbie", "https://upload.wikimedia.org/wikipedia/commons/1/1b/Herbie_car.jpg" }
                });

            migrationBuilder.InsertData(
                table: "Franchise",
                columns: new[] { "Id", "Description", "Name" },
                values: new object[,]
                {
                    { 1, "Follows the adventures of Rockatansky, a police officer in a future Australia which is experiencing societal collapse due to war and critical resource shortages.", "Mad Max" },
                    { 2, "Depicts warrant officer Ellen Ripley and her battles with an extraterrestrial lifeform, commonly referred to as \"the Alien\" or Xenomorph.", "Alien" },
                    { 3, "Herbie has a mind of his own and is capable of driving himself, and is often a serious contender in auto racing competitions.", "Herbie" }
                });

            migrationBuilder.InsertData(
                table: "Movie",
                columns: new[] { "Id", "Description", "Director", "FranchiseId", "Genre", "Picture", "ReleaseYear", "Title", "Trailer" },
                values: new object[,]
                {
                    { 1, "The Story of a lone roving warrior who is exiled into the desert and there encounters an isolated child cargo cult centred on a crashed Boeing 747 and its deceased captain.", "George Miller", 1, "Post-Apocalyptic Dystopian Action", "https://upload.wikimedia.org/wikipedia/en/e/e0/Mad_max_beyond_thunderdome.jpg", 1985, "Mad Max Beyond Thunderdome", "https://www.youtube.com/watch?v=9JKZKjFjHDM" },
                    { 2, "Follows the crew of the commercial space tug Nostromo, who, after coming across a mysterious derelict spaceship on an undiscovered moon, find themselves up against an aggressive and deadly extraterrestrial.", "Ridley Scott", 2, "Science fiction Horror", "https://upload.wikimedia.org/wikipedia/en/c/c3/Alien_movie_poster.jpg", 1979, "Alien", "https://www.youtube.com/watch?v=LjLamj-b0I8" },
                    { 3, "When communications are lost with a human colony on the moon where her crew first saw the alien creatures, Ripley agrees to return to the site with a unit of Colonial Marines to investigate.", "James Cameron", 2, "Science fiction Horror", "https://upload.wikimedia.org/wikipedia/en/f/fb/Aliens_poster.jpg", 1986, "Aliens", "https://www.youtube.com/watch?v=y5rAL5PPaSU" },
                    { 4, "Maggie Peyton, the new owner of Number 53 - the free - wheelin’ Volkswagen bug with a mind of its own - puts the car through its paces on the road to becoming a NASCAR competitor.", "Angela Robinson", 3, "Sports Comedy", "https://upload.wikimedia.org/wikipedia/en/e/e0/Herbiefl_poster.jpg", 2005, "Herbie: Fully Loaded", "https://www.youtube.com/watch?v=K0aTcuB9IPw" }
                });

            migrationBuilder.InsertData(
                table: "CharacterMovie",
                columns: new[] { "CharactersId", "MoviesId" },
                values: new object[,]
                {
                    { 1, 1 },
                    { 2, 2 },
                    { 2, 3 },
                    { 3, 4 }
                });

            migrationBuilder.CreateIndex(
                name: "IX_CharacterMovie_CharactersId",
                table: "CharacterMovie",
                column: "CharactersId");

            migrationBuilder.CreateIndex(
                name: "IX_Movie_FranchiseId",
                table: "Movie",
                column: "FranchiseId");
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "CharacterMovie");

            migrationBuilder.DropTable(
                name: "Character");

            migrationBuilder.DropTable(
                name: "Movie");

            migrationBuilder.DropTable(
                name: "Franchise");
        }
    }
}
