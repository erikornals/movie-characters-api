﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Hosting;
using MovieCharactersApi.Models.Entities;
using static System.Net.WebRequestMethods;

namespace MovieCharactersApi.Models
{
    public class MovieCharactersDbContext : DbContext
    {
        public MovieCharactersDbContext(DbContextOptions options): base(options)
        {
        }

        public DbSet<Character> Characters { get; set; }
        public DbSet<Franchise> Franchises { get; set; }
        public DbSet<Movie> Movies { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            //Seeded movies
            modelBuilder.Entity<Movie>().HasData(
                new Movie()
                {
                    Id = 1,
                    Title = "Mad Max Beyond Thunderdome",
                    Description = "The Story of a lone roving warrior who is exiled into the desert and there encounters an isolated child cargo cult centred on a crashed Boeing 747 and its deceased captain.",
                    Genre = "Post-Apocalyptic Dystopian Action",
                    ReleaseYear = 1985,
                    Director = "George Miller",
                    Picture = "https://upload.wikimedia.org/wikipedia/en/e/e0/Mad_max_beyond_thunderdome.jpg",
                    Trailer = "https://www.youtube.com/watch?v=9JKZKjFjHDM",
                    FranchiseId = 1
                },
                new Movie()
                {
                    Id = 2,
                    Title = "Alien",
                    Description = "Follows the crew of the commercial space tug Nostromo, who, after coming across a mysterious derelict spaceship on an undiscovered moon, find themselves up against an aggressive and deadly extraterrestrial.",
                    Genre = "Science fiction Horror",
                    ReleaseYear = 1979,
                    Director = "Ridley Scott",
                    Picture = "https://upload.wikimedia.org/wikipedia/en/c/c3/Alien_movie_poster.jpg",
                    Trailer = "https://www.youtube.com/watch?v=LjLamj-b0I8",
                    FranchiseId = 2
                },
                new Movie()
                {
                    Id = 3,
                    Title = "Aliens",
                    Description = "When communications are lost with a human colony on the moon where her crew first saw the alien creatures, Ripley agrees to return to the site with a unit of Colonial Marines to investigate.",
                    Genre = "Science fiction Horror",
                    ReleaseYear = 1986,
                    Director = "James Cameron",
                    Picture = "https://upload.wikimedia.org/wikipedia/en/f/fb/Aliens_poster.jpg",
                    Trailer = "https://www.youtube.com/watch?v=y5rAL5PPaSU",
                    FranchiseId = 2
                },
                new Movie()
                {
                    Id = 4,
                    Title = "Herbie: Fully Loaded",
                    Description = "Maggie Peyton, the new owner of Number 53 - the free - wheelin’ Volkswagen bug with a mind of its own - puts the car through its paces on the road to becoming a NASCAR competitor.",
                    Genre = "Sports Comedy",
                    ReleaseYear = 2005,
                    Director = "Angela Robinson",
                    Picture = "https://upload.wikimedia.org/wikipedia/en/e/e0/Herbiefl_poster.jpg",
                    Trailer = "https://www.youtube.com/watch?v=K0aTcuB9IPw",
                    FranchiseId = 3
                }
            );
            // Seeded characters
            modelBuilder.Entity<Character>().HasData(
                new Character()
                {
                    Id = 1,
                    Name = "Max Rockatansky",
                    Alias = "Mad Max",
                    Gender = "Male",
                    Picture = "https://upload.wikimedia.org/wikipedia/commons/8/80/Long_Beach_Comic_%26_Horror_Con_2011_-_Mad_Max_%286301702996%29.jpg",
                },
                new Character()
                {
                    Id = 2,
                    Name = "Lt. Ellen Louise Ripley",
                    Alias = "Ripley",
                    Gender = "Female",
                    Picture = "https://upload.wikimedia.org/wikipedia/en/b/b9/Ellen_Ripley_%28Alien_-_Isolation%29.jpg"
                },
                new Character()
                {
                    Id = 3,
                    Name = "Herbie",
                    Alias = "The Love Bug",
                    Gender = "Car",
                    Picture = "https://upload.wikimedia.org/wikipedia/commons/1/1b/Herbie_car.jpg"
                }
            );
            // Seeded Franchises
            modelBuilder.Entity<Franchise>().HasData(
                new Franchise()
                {
                    Id = 1,
                    Name = "Mad Max",
                    Description = "Follows the adventures of Rockatansky, a police officer in a future Australia which is experiencing societal collapse due to war and critical resource shortages.",
                },
                new Franchise()
                {
                    Id = 2,
                    Name = "Alien",
                    Description = "Depicts warrant officer Ellen Ripley and her battles with an extraterrestrial lifeform, commonly referred to as \"the Alien\" or Xenomorph.",
                },
                new Franchise()
                {
                    Id = 3,
                    Name = "Herbie",
                    Description = "Herbie has a mind of his own and is capable of driving himself, and is often a serious contender in auto racing competitions.",
                }
            );
            //Seeded CharacterMovies
            modelBuilder.Entity<Character>()
                .HasMany(c => c.Movies)
                .WithMany(m => m.Characters)
                .UsingEntity<Dictionary<string, object>>(
                    "CharacterMovie",
                    r => r.HasOne<Movie>().WithMany().HasForeignKey("MoviesId"),
                    l => l.HasOne<Character>().WithMany().HasForeignKey("CharactersId"),
                    je =>
                    {
                        je.HasKey("MoviesId", "CharactersId");
                        je.HasData(
                            new { MoviesId = 1, CharactersId = 1 },
                            new { MoviesId = 2, CharactersId = 2 },
                            new { MoviesId = 3, CharactersId = 2 },
                            new { MoviesId = 4, CharactersId = 3 });
                    });
        }
    }
}
