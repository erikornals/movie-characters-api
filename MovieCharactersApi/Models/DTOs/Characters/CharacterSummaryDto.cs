﻿using System.ComponentModel.DataAnnotations;

namespace MovieCharactersApi.Models.DTOs.Characters
{
    public class CharacterSummaryDTO
    {
        public int Id { get; set; }
        public string Name { get; set; } = null!;
        public string? Alias { get; set; }
        public string? Gender { get; set; }
        public string? Picture { get; set; }
    }
}
