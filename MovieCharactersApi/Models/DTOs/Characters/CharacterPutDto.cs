﻿namespace MovieCharactersApi.Models.DTOs.Characters
{
    /// <summary>
    /// Datatransferobject for updating a character. Related data seperate endpoint.
    /// </summary>
    public class CharacterPutDto
    {
        public int Id { get; set; }
        public string Name { get; set; } = null!;
        public string Alias { get; set; } = null!;
        public string Gender { get; set; } = null!;
        public string Picture { get; set; } = null!;
    }
} 
