﻿namespace MovieCharactersApi.Models.DTOs.Characters
{   
    /// <summary>
    /// Datatransferobject for character (lists of movies ids)
    /// </summary>
    public class CharacterDto
    {
        // id, name, alias, gender, picture
        // movies
        public int Id { get; set; }
        public string Name { get; set; } = null!;
        public string Alias { get; set; } = null!;
        public string Gender { get; set; } = null!;
        public string Picture { get; set; } = null!;
        public List<int>? Movies { get; set; }
    }
}
