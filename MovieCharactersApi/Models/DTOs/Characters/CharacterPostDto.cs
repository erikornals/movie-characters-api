﻿using System.ComponentModel.DataAnnotations;

namespace MovieCharactersApi.Models.DTOs.Characters
{
    /// <summary>
    /// DTO for posting a new character based on id.
    /// Related data: separated endpoint
    /// </summary>
    public class CharacterPostDto
    {
        public string Name { get; set; } = null!;
        public string Alias { get; set; } = null!;
        public string Gender { get; set; } = null!;
        public string Picture { get; set; } = null!;
    }
}
