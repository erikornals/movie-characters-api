﻿using MovieCharactersApi.Models.Entities;
using System.ComponentModel.DataAnnotations;

namespace MovieCharactersApi.Models.DTO.Movie
{
    public class MovieDTO
    {
        public int Id { get; set; }
        public string Title { get; set; } = null!;
        public string? Description { get; set; } 
        public string? Genre { get; set; }
        public int ReleaseYear { get; set; }
        public string Director { get; set; } = null!;
        public string? Picture { get; set; }
        public string? Trailer { get; set; }
        public int? Franchise { get; set; }
        public List<int>? Characters { get; set; }
    }
}