﻿using System.ComponentModel.DataAnnotations;

namespace MovieCharactersApi.Models.DTOs.Franchises
{
    public class FranchisePostDto
    {
        public string? Name { get; set; } = null!;
        public string? Description { get; set; }
    }
}
