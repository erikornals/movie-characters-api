﻿using MovieCharactersApi.Models.Entities;

namespace MovieCharactersApi.Models.DTOs.Franchises
{
    public class FranchiseDto
    {
        public int Id { get; set; }
        public string Name { get; set; } = null!;
        public string Description { get; set; } = null!;

        // Relationships
        public List<int>? Movies { get; set; }
    }
}

