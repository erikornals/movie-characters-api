﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace MovieCharactersApi.Models.Entities
{
    [Table("Character")]
    public class Character
    {
        public int Id { get; set; }
        [MaxLength(50)]
        public string Name { get; set; } = null!;
        [MaxLength(50)]
        public string? Alias { get; set; }
        [MaxLength(50)]
        public string? Gender { get; set; }
        public string? Picture { get; set; }

        // Relationships
        public ICollection<Movie>? Movies { get; set; }
    }
}
