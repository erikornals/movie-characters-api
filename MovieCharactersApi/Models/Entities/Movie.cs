﻿using Microsoft.Extensions.Hosting;
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace MovieCharactersApi.Models.Entities
{
    [Table("Movie")]
    public class Movie
    {
        public int Id { get; set; }
        [MaxLength(50)]
        public string Title { get; set; } = null!;
        [MaxLength(500)]
        public string? Description { get; set; }
        [MaxLength(50)]
        public string? Genre { get; set; } // string of comma-separated genres
        public int ReleaseYear { get; set; }
        [MaxLength(50)]
        public string Director { get; set; } = null!;
        public string? Picture { get; set; } // movie poster URL
        public string? Trailer { get; set; } // Youtube link 

        // Relationships
        public int? FranchiseId { get; set; }
        public Franchise? Franchise { get; set; }
        public ICollection<Character>? Characters { get; set; }
    }
}
