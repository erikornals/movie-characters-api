﻿using Microsoft.Identity.Client;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace MovieCharactersApi.Models.Entities
{
    [Table("Franchise")]
    public class Franchise
    {
        public int Id { get; set; }
        [MaxLength(50)]
        public string? Name { get; set; } = null!;
        [MaxLength(500)]
        public string? Description { get; set; }

        // Relationships
        public ICollection<Movie>? Movies { get; set; }
    }
}
